import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router";
import OnlyForm from "./OnlyForm";
import MainHeader from "./MainHeader";

export default class FmRUHead extends Component
{
	render() {

		return (
			<Switch>
				<Route path='/' component={OnlyForm}/>
				<Route path='/page' component={MainHeader}/>
				<Route path='/fmru_player' component={MainHeader}/>
			</Switch>
		)

	}
};
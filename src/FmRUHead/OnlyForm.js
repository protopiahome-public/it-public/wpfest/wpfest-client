import React, {Component, Fragment} from 'react';
import FmRULoginWindow from "./FmRULoginWindow";
import FmRURegisterWindow from "./FmRURegisterWindow";
import FmRUFilterWindow from "./FmRUFilterWindow";
import FmRUCountWindow from "./FmRUCountWindow";

export default class OnlyForm extends Component
{

    render()
    {
        return (
            <Fragment>
            <FmRULoginWindow  login = {this.props.login} prnt={this.props.prnt}/>
            <FmRURegisterWindow  register = {this.props.register} prnt={this.props.prnt}/>
            <FmRUFilterWindow data={this.props.p} prnt={this.props.prnt} onClick = {this.props.onItemClick}/>
            <FmRUCountWindow data={this.props.p} prnt={this.props.prnt} onClick = {this.props.onItemClick}/>
        </Fragment>
        );
    }
}
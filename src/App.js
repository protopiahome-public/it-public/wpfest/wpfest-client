import React, {Component, Fragment} from 'react';
import {deleteCookie, getCookie} from "./FmRUAppExt";
import Foo from "./Foo";
import Aalert from "./Aalert";
import FmRUPhase from "./Main/FmRUPlayer/FmRUPhase";
import FmRUUser from "./Main/FmRUUserPage/FmRUUser";
import Loader from "./Loader";

import Body from "./Body";
import FmRUApp from "./FmRUApp";
import * as actions from "./actions/actions";
import {connect} from "react-redux";

class App extends Component
{

    constructor(props)
    {
        super(props);
        var lass = getCookie("l");
        var l = lass ? lass.split("&") : [] ;
        var log ="";
        var psw = "";
        if(l.length)
        {
            log = l[0];
            psw = l[1];
        }
        this.state =
            {
                code: "",
                args: "",
                token: "",
                log: log,
                psw: psw,
                data:{},
                gfilter:[],
                mpp:props.mpp,
                mtype:props.mtype
            };
        this.alert = React.createRef();
        this.loader = React.createRef();

    }
    componentDidMount ()
    {
        Foo.app 		= this;
        Foo.is_comment	= true;
        Foo.alert		= <Aalert
            title={""}
            content={"!!"}
            textarea=""
            okTitle={"ok"}
            escTitle={"cancel"}
            okHandler={function(){}}
            escHandler={function(){}}
            checkTitle={""}
            ref = {this.alert}
        />;
    }
    componentDidUpdate()
    {
        if(this.props.data.prompt)
        {
            var data = this.props.data.prompt;
            this.a_alert( typeof data === "string" ? {content:data} : data );
        }
    }
    render()
    {
        const loader = <Loader/>;
        FmRUPhase.setPhase(this.props.data.status);
        Foo.is_expert = this.props.data.is_expert;
        FmRUUser.instance = <FmRUUser
            name = {this.props.data.name }
            user_id = {this.props.data.user_id }
            is_expert = {this.props.data.is_expert }
            roles = {this.props.data.roles }
        />
        FmRUApp.user = Foo.app;


        //this.props.code
        return <Fragment>
            {Foo.alert}

            <Body>

            </Body>


            {loader}
        </Fragment>;
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    onPropClick : ( code, data, args, log, psw ) =>
        dispatch(
            actions.onPropClick( code, data, args, log, psw )
        ),
    //onGfilter: (ganres, gfilter) => dispatch(actions.onGfilter( ganres, gfilter )),
})

export default connect( mapStateToProps, mapDispatchToProps )(App);

//                deleteCookie("l");
//                 //this.state.log = "";
//                 //this.state.psw = "";
//                 //this.fetch( "invalid_username", 0);
//                 cont = (<Page404 code={"invalide_username"} message={"invalid username"}></Page404>)
import {React, Component} from 'react';
import PropTypes from "prop-types";
import Voc from "../components/Voc";

class InvalideUsernamePage extends Component
{

    render()
    {
        const code = this.props.code;
        const message = this.props.message;
        return (
            <div className="container text-center">
            <div className="jumbotron align-items-center">
                <div className="row  justify-content-md-center  justify-content-sm-center">
                    <div className="col-md-12 display-4">
                        <Voc text={message} />
                    </div>
                    <div className="spacer-30" />
                    <div
                        className="btn btn-primary "
                        onClick={(evt)=>this.resetHandler(evt)}
                    >
                        {"Вернуться на первую"}
                    </div>
                </div>
            </div>
        </div>);
    }
}

InvalideUsernamePage.propTypes = {
    code: PropTypes.string,
    message: PropTypes.string,
}

InvalideUsernamePage.defaultProps = {
    code: "",
    message: ""
};

export default InvalideUsernamePage;
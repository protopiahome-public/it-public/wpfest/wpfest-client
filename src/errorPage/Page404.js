import {React, Component} from 'react';
import Voc from "../components/Voc";

import PropTypes from 'prop-types';

class Page404 extends Component
{

    render()
    {
        const code = this.props.code;
        const message = this.props.message;

        return (
            <div className="container text-center">
                <div className="jumbotron align-items-center">
                    <div className="row  justify-content-md-center  justify-content-sm-center">
                        <div className="display-1 col-md-12">
                            {code}
                        </div>
                        <div className="col-md-12">
                            {message}
                        </div>
                        <div className="spacer-30" />
                        <div
                            className="btn btn-primary "
                            data-fmru_type="page"
                            data-args="1"
                            onClick={(evt)=>this.onMemberClick(evt)}
                        >
                            {"Вернуться на первую"}
                        </div>
                    </div>
                </div>
            </div>

        );

    }
}

Page404.propTypes = {
    code: PropTypes.string,
    message: PropTypes.string,
}

Page404.defaultProps = {
    code: "",
    message: ""
};

export default Page404;
import {Component} from 'react';
import PropTypes from "prop-types";

class DefaultErrorPage extends Component
{

    render()
    {
        const code = this.props.code;
        const message = this.props.message;

        return (
            <div className="container colored text-center">
            <section>
                <div className="row">
                    <div className="display-1 col-md-12">
                        {code}
                    </div>
                    <div className="col-md-12">
                        {message}
                    </div>
                    <div className="spacer-30" />
                    <div
                        className="btn btn-primary "
                        data-fmru_type="page"
                        data-args="1"
                        onClick={(evt)=>this.onMemberClick(evt)}
                    >
                        {"Вернуться на первую"}
                    </div>
                </div>
            </section>
        </div>);
    }
}

DefaultErrorPage.propTypes = {
    code: PropTypes.string,
    message: PropTypes.string,
}

DefaultErrorPage.defaultProps = {
    code: "",
    message: ""
};

export default DefaultErrorPage;
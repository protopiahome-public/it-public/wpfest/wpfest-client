import React, {Component} from 'react';
import FmRULoginForm from "../../FmRUHead/FmRULoginForm";
import {Link} from "react-router-dom";
import icon1 from "../../img/test.svg";
import FmRUPhase from "./FmRUPhase";
import FmRUMemberPage from "./FmRUMemberPage";
import Voc from "../../components/Voc";

export default class FmRUPlayer extends Component
{

    render()
    {
        const about = this.props.data.enabled_rules ? <div
            className="indicator classic"
            id="indicator_about"
            data-fmru_type="page"
            data-args="0"
            onClick={this.onMemberClick}
        >
            <div className="n1"><Voc text={"About"} /></div>
            <div className="iconnn">
                <img src={ this.props.data.logo } alt="" />
            </div>
        </div> : "" ;
        return (
                <section>
                    <div className="carousel-indicators">
                        {about}
                        <FmRULoginForm
                            prnt={ this }
                            user = { this.props.data }
                            onUser = { this.onUser }
                            login = { this.login.bind(this) }
                            onLogout={this.onLogout }
                            is_register={this.props.data.is_register}
                        />
                        <Link to="/">
                            <div
                                className="indicator classic"
                                id="indicator_to_list"
                                data-fmru_type="list"
                                data-args="0"
                                onClick = { this.onMemberClick.bind(this) }
                            >
                                <div className="n1"><Voc text={"To list"} /></div>
                                <div className="iconnn">
                                    <img src={ icon1 } alt=''/>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className="row">
                        <div className="col-12 text-center">
                            <div className="spacer-10" />
                            {FmRUPhase.getText()}
                        </div>
                    </div>
                    <FmRUMemberPage
                        mdata={this.props.data}
                        onItemClick = {this.onMemberClick.bind(this)}
                    />
                </section>
        );
    }
}
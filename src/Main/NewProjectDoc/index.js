import React, {Component} from 'react';
import Vocabulary from '../../components/Vocabulary';
import DrawingState from "./DrawingState";

export default class NewProjectDoc extends Component
{

    render()
    {
        return (
                <div className="row justify-content-center">
                    <div className="col-lg-6 col-md-8 col-sm-12">
                        <DrawingState
                            width = {600}
                            height = {200}
                            id="Drawing"
                        />
                        <div
                            className="btn btn-link"
                        >
                            { Vocabulary.getText("send") }
                        </div>
                    </div>
                </div>
        );
    }
}
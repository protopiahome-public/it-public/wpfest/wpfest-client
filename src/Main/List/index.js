import React, {Component, Fragment} from 'react';
import Voc from "../../components/Voc";
import icon3 from "../../img/tools-and-utensils-1.svg";
import Vocabulary from "../../components/Vocabulary";
import FmRUUser from "../FmRUUserPage/FmRUUser";
import FmRUMemberBox from "../Page/FmRUMemberBox";
import FmRULoginForm from "../../FmRUHead/FmRULoginForm";
import icon2 from "../../img/kabuki.svg";
import FmRUPhase from "../FmRUPlayer/FmRUPhase";
import FmRUPagi from "../FmRUPagi";

export default class List extends Component
{

    render()
    {
        const countChooser = this.props.data.count_chooser ?
            <div
                className="indicator classic"
                id='indicator_count'
                data-toggle='modal'
                data-target='#countModal'
            >
                <div className="n1"><Voc text={"Count"} /></div>
                <div className="iconnn">
                    <img src={ icon3 } alt=''/>
                </div>
            </div>: "";
        const user_descr = this.props.data.roles.map(elem =>
            <div
                className="role_descr"
                key={"roledescr_"+elem }
                dangerouslySetInnerHTML={{ __html : Vocabulary.getText(FmRUUser.bySlug(elem)[0].descr) }}
            />
        );
        const about = this.props.data.enabled_rules ? <div
            className="indicator classic"
            id="indicator_about"
            data-fmru_type="page"
            data-args="0"
            onClick={this.onMemberClick}
        >
            <div className="n1"><Voc text={"About"} /></div>
            <div className="iconnn">
                <img src={ this.props.data.logo } alt="" />
            </div>
        </div> : "" ;
        if(user_descr.length === 0)
            user_descr.push(
                <div
                    className="role_descr"
                    key={"roledescr_contributor" }
                    dangerouslySetInnerHTML={{ __html : Vocabulary.getText(FmRUUser.bySlug("contributor")[0].descr)}}
                /> );
        const my_projects = this.props.data.my_members.length ? <Fragment>
            <section style={{background:"rgba(0,0,0,0.2)"}}>
                <div className='colored  not'>
                    <div className="row">
                        <div className="col-md-12">
                            <ul className="pagination justify-content-center">
                                <li className="page-item">
                                    <a className="page-link"><strong>
                                        {Vocabulary.getText("My projects")}
                                    </strong></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="spacer-10"/>
                    < FmRUMemberBox
                        members={this.props.data.my_members}
                        onItemClick = {this.onMemberClick.bind(this)}
                        mtype = {"card"}
                    />
                </div>
            </section>
        </Fragment>: "";

        //    особое внимание, несколько container colored из первого удален FmRUHead
        return (
            <Fragment>
                <div className='container colored'>

                    <section>
                        <div className="carousel-indicators">
                            {about}
                            <FmRULoginForm
                                prnt={ this }
                                user = { this.props.data }
                                onUser = { this.onUser }
                                login = { this.login.bind(this) }
                                onLogout={this.onLogout }
                                is_register={this.props.data.is_register}
                            />
                            <div
                                className="indicator classic"
                                id="indicator_ganres"
                                data-toggle='modal'
                                data-target='#filterModal'
                            >
                                <div className="n1"><Voc text={"Ganres"} /></div>
                                <div className="iconnn">
                                    <img src={ icon2 } alt="" />
                                </div>
                            </div>
                            {countChooser}
                        </div>
                        <div className="row">
                            <div className="col-12 text-center">
                                <div className="spacer-10" />
                                {FmRUPhase.getText()}
                            </div>
                        </div>
                    </section>
                </div>
                {my_projects}
                <div className='container colored'>
                    <section>
                        <div className="row">
                            <FmRUPagi
                                data={ this.props.data.pagi }
                                onItemClick = {this.onMemberClick.bind(this)}
                                colm={this.props.data.colm}
                                title={Vocabulary.getText("Members")}
                                prefix={"page_"}
                                fmru_type="list"
                            />
                        </div>
                        < FmRUMemberBox
                            members={this.props.data.members}
                            onItemClick = {this.onMemberClick.bind(this)}
                            mtype = {this.props.data.mtype}
                        />
                        <div className="row">
                            <FmRUPagi
                                data={ this.props.data.pagi }
                                onItemClick = {this.onMemberClick.bind(this)}
                                colm={this.props.data.colm}
                                title={Vocabulary.getText("Members")}
                                prefix={"page_"}
                                fmru_type="list"
                            />
                        </div>
                    </section>
                </div>
                <div className='container colored'>
                    <section style={{backgroundColor:"rgba(0,0,0,0.75)", color:"#FFF"}}>
                        <div className="row justify-content-start">
                            <div className="col-lg-6 col-xm-6 col-md-6 col-sm-12 col-12 align-self-center">
                                {user_descr}
                            </div>
                        </div>
                    </section>
                </div>
            </Fragment>);
    }
}
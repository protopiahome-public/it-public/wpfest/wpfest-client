import React, {Component, Fragment} from 'react';
import FmRUPhase from "../FmRUPlayer/FmRUPhase";
import Vocabulary from "../../components/Vocabulary";
import FmRUMemberBox from "./FmRUMemberBox";
import FmRUHead from "../../FmRUHead";
import Voc from "../../components/Voc";
import FmRULoginForm from "../../FmRUHead/FmRULoginForm";
import icon1 from "../../img/test.svg";
import FmRUPagi from "../FmRUPagi";

export default class Page extends Component
{

    render()
    {
        console.log( this.props.data );
        const posts		= this.props.data.posts.map(elem => (
            <div className="row" key={elem.id}>
                <div className="col-12">
                    <div className="diary_post mt-2">
                        <div className="diary_title">
                            {elem.post_title}
                        </div>
                        <div className="diary_body"
                             dangerouslySetInnerHTML={{ __html : elem.post_content }}
                        />
                        <div className="diary_footer">
                            <span> <i className="fas fa-clock" 	style={{opacity:0.5}}></i> {elem.post_date} </span>
                            <span> <i className="fas fa-user"	style={{opacity:0.5}}></i> {elem.post_author} </span>
                            <a
                                onClick={this.onMemberClick}
                                data-fmru_type="fmru_player"
                                data-args={elem.prid}
                            >
                                <i className="fas fa-folder"	style={{opacity:0.5}}></i> {elem.diary}
                            </a>
                        </div>
                    </div>
                </div>
                <div className="spacer-30"/>
            </div>
        ))
        const crMyPrForm =<div className="row justify-content-center">
            <div className="col-lg-6 col-md-8 col-sm-12 mt-5">
                <div className='lead font-weight-bold text-center'>
                    {this.props.data.title}
                </div>
                <div className="spacer-30" />
                {FmRUPhase.getText()}
                <div className="spacer-10" />
                <div
                    dangerouslySetInnerHTML={{ __html : this.props.data.content}}
                />
            </div>
        </div>
        const my_projects = this.props.data.my_members.length ?
            <Fragment>
                <section style={{background:"rgba(0,0,0,0.2)"}}>
                    <div className='colored  not'>
                        <div className="row">
                            <div className="col-md-12">
                                <ul className="pagination justify-content-center">
                                    <li className="page-item">
                                        <a className="page-link"><strong>
                                            {Vocabulary.getText("My projects")}
                                        </strong></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="spacer-10"/>
                        < FmRUMemberBox
                            members={this.props.data.my_members}
                            onItemClick = {this.onMemberClick }
                            mtype = {"card"}
                        />
                    </div>
                </section>
            </Fragment> : null;

        //    особое внимание, несколько container colored из первого удален FmRUHead
        return (
            <Fragment>
                <div className='container colored'>
                    <section>
                        <div className="carousel-indicators">
                            <div
                                className="indicator classic"
                                id='indicator_about'
                                data-fmru_type="page"
                                data-args="0"
                                onClick={this.onMemberClick}
                            >
                                <div className="n1"><Voc text={"About"} /></div>
                                <div className="iconnn">
                                    <img src={ this.props.data.logo } alt="" />
                                </div>
                            </div>
                            <FmRULoginForm
                                prnt={ this }
                                user = { this.props.data }
                                onUser = { this.onUser }
                                login = { this.login.bind(this) }
                                onLogout={this.onLogout }
                                is_register={this.props.data.is_register}
                            />
                            <div
                                className="indicator classic"
                                id='indicator_members'
                                data-fmru_type="list"
                                data-args="0"
                                onClick={this.onMemberClick}
                            >
                                <div className="n1"><Voc text={"Members"} /></div>
                                <div className="iconnn">
                                    <img src={ icon1 } alt="" />
                                </div>
                            </div>
                        </div>
                        {crMyPrForm}
                        {
                            this.props.data.user_id < 1 ? null :
                                <div className="row justify-content-center">
                                    <div className="col-lg-6 col-md-8 col-sm-12 text-center">
                                        <div className="spacer-30" />
                                        <div
                                            className='btn btn-primary newProjectDoc'
                                            data-fmru_type="newProjectDoc"
                                            data-args="0"
                                            onClick={ this.onMemberClick }
                                        >
                                            {Vocabulary.getText("create new Project")}
                                        </div>
                                    </div>
                                </div>
                        }
                    </section>
                </div>
                {my_projects}
                <div className='container colored'>
                    <section>
                        <div className="row">
                            <FmRUPagi
                                data={ this.props.data.pagi }
                                onItemClick = {this.onMemberClick.bind(this)}
                                colm={this.props.data.colm}
                                title={Vocabulary.getText("Last posts")}
                                prefix={"posts_"}
                                fmru_type="page"
                            />
                        </div>
                        {posts}
                        <div className="row">
                            <div className="spacer-10" />
                            <FmRUPagi
                                data={ this.props.data.pagi }
                                onItemClick = {this.onMemberClick.bind(this)}
                                colm={this.props.data.colm}
                                title={Vocabulary.getText("Last posts")}
                                prefix={"posts_"}
                                fmru_type="page"
                            />
                        </div>
                    </section>
                </div>
            </Fragment>)
    }
}
import React, {Component} from 'react';
import FmRUHead from "./FmRUHead";
import {Route, Switch} from "react-router";
import Nothing from "./Main/Nothing";
import Page404 from "./errorPage/Page404";
import NewProjectDoc from "./Main/NewProjectDoc";
import FmRUPlayer from "./Main/FmRUPlayer";
import List from "./Main/List";
import Page from "./Main/Page";
import FmRUUserPage from "./Main/FmRUUserPage";
import InvalideUsernamePage from "./errorPage/InvalideUsernamePage";

export default class Body extends Component
{

    //USER container colored not
    //                < FmRUHead
    //                     p={this.props.data}
    //                     prnt={this}
    //                     page_type = "page"
    //                     onItemClick = {this.onMemberClick.bind(this)}
    //                     login = {this.login.bind(this)}
    //                     register={this.onRegister}
    //                     onLogout = {this.onLogout}
    //                     onUser = {this.onUser}
    //                 />
    // <FmRUUserPage data={this.props.data} prnt={this} onFirst={ this.onMemberClick.bind(this) }  />
    // FmRUPlayer container colored
    //                < FmRUHead
    //                     p={this.props.data}
    //                     prnt={this}
    //                     page_type = "page"
    //                     onItemClick = {this.onMemberClick.bind(this)}
    //                     login = {this.login.bind(this)}
    //                     register={this.onRegister}
    //                     onLogout = {this.onLogout}
    //                     onUser = {this.onUser}
    //                 />
    //
    //NewProjectDoc container colored not
    //                 < FmRUHead
    //                     p={this.props.data}
    //                     prnt={this}
    //                     page_type = "page"
    //                     onItemClick = {this.onMemberClick.bind(this)}
    //                     login = {this.login.bind(this)}
    //                     register={this.onRegister}
    //                     onLogout = {this.onLogout}
    //                     onUser = {this.onUser}
    //                 />
    // Page container colored
    //                     < FmRUHead
    //                         p={this.props.data}
    //                         prnt={this}
    //                         page_type = "page"
    //                         onItemClick = {this.onMemberClick}
    //                         login = {this.login}
    //                         register={this.onRegister}
    //                         onLogout = {this.onLogout}
    //                         onUser = {this.onUser}
    //                     />
    // List container colored
    //                    < FmRUHead
    //                         p={this.props.data}
    //                         prnt={this}
    //                         page_type = "page"
    //                         onItemClick = {this.onMemberClick.bind(this)}
    //                         login = {this.login.bind(this)}
    //                         register={this.onRegister}
    //                         onLogout = {this.onLogout}
    //                         onUser = {this.onUser}
    //                     />
    render()
    {
        return (
            <div className='container colored not'>
                < FmRUHead
                    p={this.props.data}
                    prnt={this}
                    page_type = "page"
                    onItemClick = {this.onMemberClick}
                    login = {this.login}
                    register={this.onRegister}
                    onLogout = {this.onLogout}
                    onUser = {this.onUser}
                />
                <Switch>
                    <Route path='/' component={Nothing}/>
                    <Route path="/rest_no_route" render={(data) => <Page404 code={this.props.data.data.status} message={this.props.data.message}></Page404>}/>
                    <Route path="/incorrect_password" render={(data) => <InvalideUsernamePage code={"invalide_username"} message={"invalid username"}></InvalideUsernamePage>}/>
                    <Route path="/invalid_username" render={(data) => <InvalideUsernamePage code={"invalide_username"} message={"invalid username"}></InvalideUsernamePage>}/>
                    <Route path='/list' component={List}/>
                    <Route path='/page' component={Page}/>
                    <Route path='/newProjectDoc' component={NewProjectDoc}/>
                    <Route path='/fmru_player' component={FmRUPlayer}/>
                    <Route path='/user' component={FmRUUserPage}/>
                </Switch>
            </div>
        );
    }
}